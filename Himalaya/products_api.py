#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
product_api module is a FastAPI module which export an api to interact with products_db
"""
from datetime import date
from typing import Optional
from fastapi import FastAPI
from Himalaya.products.data_types import MessageDataType, PhoneDataType, GroupDataType
from Himalaya.products.db_handler import DBHandler


db_handler = DBHandler.initialize()

app = FastAPI()


@app.get("/message/{id}")
def get_message_by_id(id: int):
    return db_handler.get_object_by_id("message", id)


@app.get("/phone/{id}")
def get_phone_by_id(id: int):
    return db_handler.get_object_by_id("phone", id)


@app.get("/group/{id}")
def get_group_by_id(id: int):
    return db_handler.get_object_by_id("group", id)


@app.get("/messages/")
def get_messages(
        src_id: Optional[int] = None,
        data_type: Optional[str] = None,
        from_date: Optional[date] = None,
        to_date: Optional[date] = None
):
    """
    get messages that passes specific filters
    :param src_id: (optional) source id
    :param data_type: (optional) data type
    :param from_date: (optional) minimum receive time
    :param to_date:  (optional) maximum recieve time
    :return: all the messages that passed the filters
    """
    return db_handler.get_messages(src_id, data_type, from_date, to_date)


@app.post("/insert/message/")
def add_message(msg: MessageDataType):
    """
    adds a new message
    :param msg: message params
    :return: status of creation
    """
    return db_handler.add_message(msg)


@app.post("/insert/phone/")
def add_phone(p: PhoneDataType):
    """
    adds a new phone
    :param p: phone params
    :return: status of creation
    """
    return db_handler.add_phone(p)


@app.post("/insert/group/")
def add_phone(g: GroupDataType):
    """
    adds a new group
    :param g: group params
    :return: status of creation
    """
    return db_handler.add_group(g)