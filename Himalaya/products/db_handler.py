#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
The db_handler module makes interface for API's to access the products DB
"""
from datetime import date

from sqlalchemy import create_engine
from sqlalchemy import sessionmaker

from Himalaya.config import DB_ENGINE
from Himalaya.products.products_db import Phone, Message, Group
from Himalaya.products.data_types import MessageDataType


class DBHandler(object):

    DB_OBJECTS = {
        'message': Message,
        'phone': Phone,
        'group': Group
    }
    """
    This class create a session to the Products DB and creates an interface to access it
    """

    def __init__(self, Session):
        self.Session = Session

    @classmethod
    def initialize(cls):
        """
        class method that initialize a engine and create a session
        :return: DBHandler object
        """
        engine = create_engine(DB_ENGINE, echo=True)
        Session = sessionmaker(bind=engine)
        return cls(Session)

    def get_object_by_id(self, obj_name, id):
        """
        get specific object by id
        :param obj_name: one of the database objects
        :param id: id of the object
        :return: the object with the id
        """
        obj_type = self.DB_OBJECTS.get(obj_name)
        if obj_type is None:
            return {'status': f'no such object {obj_name}'}

        session = self.Session()
        obj = session.query(obj_type).filter(obj_type.id == id).first()
        session.close()
        if obj is None:
            return {'status': f'no {obj_name} found with id={id}'}
        return obj.show()

    def get_messages(self, src_id: int = None, data_type: str = None, from_date: date = None,
                     to_date: date = None):
        """
        gets messages by zero or more filters
        without filters will return all messages
        :param src_id: (optional) filter by source id
        :param data_type: (optional) filter by data type
        :param from_date: (optional) filter by minimum date
        :param to_date: (optional) filter by maximum date
        :return: the messages that passed the filters
        """

        session = self.Session()
        messages = session.query(Message).filter(
            (src_id is None or Message.source_id == src_id) and
            (data_type is None or Message.data_type == data_type) and
            (from_date is None or Message.receive_time > from_date) and
            (to_date is None or Message.receive_time < from_date)
        ).all()
        if messages is []:
            return {'status': 'no message found for the filter'}
        return {message.id: message.show() for message in messages}


    def add_message(self, msg: MessageDataType):
        """
        adds a message to the db
        :param message: message params
        :return: the status of creation
        """
        session = self.Session()
        try:
            message = Message()
            message.id = msg.id
            message.source_id = msg.src_id
            message.receive_time = msg.receive_time
            message.data_type = msg.data_type
            message.msg_metadata = msg.metadata
            session.add(message)
            session.commit()
        except Exception:
            status = 'failed to add the message'
        session.close()
        status = "succeed"
        return {'status': status}
