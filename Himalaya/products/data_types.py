#!/usr/bin/python
# -*- coding: utf-8 -*-
from datetime import datetime

from pydantic import BaseModel


class MessageDataType(BaseModel):
    """
    This is the message base model
    """
    id: int
    src_id: int
    data_type: str
    receive_time: datetime
    metadata: str = None


class MessageSourceDataType(BaseModel):
    """
    This is the message source base model
    """
    id: int


class PhoneDataType(MessageSourceDataType):
    """
    This is the phone base model
    """
    phone_num: str
    operation_system = str
    group_id: int


class GroupDataType(BaseModel):
    """
    This is the group base model
    """
    id: int
    name: str
