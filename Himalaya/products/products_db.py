#!/usr/bin/python
# -*- coding: utf-8 -*-
from sqlalchemy import relationship
from sqlalchemy import Column, String, Integer, ForeignKey, DateTime
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class MessageSource(Base):
    __tablename__ = "sources"

    id = Column("id", Integer, primary_key=True)
    messages = relationship("Message")

    def show(self):
        return {
            "id": self.id,
        }


class Message(Base):
    __tablename__ = "messages"

    id = Column("id", Integer, primary_key=True)
    receive_time = Column("receive_time", DateTime)
    content = Column("content", String)
    data_type = Column("data_type", String)
    source_id = Column("source_id", ForeignKey(MessageSource.id))
    msg_metadata = Column("msg_metadata", String, default="")

    def show(self):
        return {
            "id": self.id,
            "receive_time": self.receive_time,
            "data_type": self.data_type,
            "source": self.source_id,
            "msg_metadata": self.msg_metadata,
            "content": self.content
        }


class Phone(MessageSource):

    phone_num = Column("phone_num", String)
    operation_system = Column("operation_system", String)
    group_id = Column(Integer, ForeignKey('groups.id'))

    def show(self):
        return {
            **super().show(),
            **dict(phone_num=self.phone_num, operation_system=self.operation_system, group_id=self.group_id)
        }


class Group(Base):
    __tablename__ = "groups"

    id = Column("id", Integer, primary_key=True)
    name = Column("name", String)
    phones = relationship("Phone")

    def show(self):
        return {
            "id": self.id,
            "name": self.name,
        }