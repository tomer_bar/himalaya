#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Config module contains database configurations
"""
DB_SERVER_NAME = 'localhost'
DB_NAME = 'products'
DB_USERNAME = 'SA'
DB_PASSWORD = 'SuperPassword1!'

DB_ENGINE = fr'mssql+pyodbc://{DB_USERNAME}:{DB_PASSWORD}@{DB_SERVER_NAME}/{DB_NAME}?driver=SQL+Server+Native+Client+11.0'
