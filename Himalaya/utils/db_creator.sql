USE master
GO

IF	DB_ID('Products') IS NOT NULL
DROP DATABASE Products;
GO

CREATE DATABASE Products;
GO

USE Products;

-- Creating Tables --
CREATE TABLE groups (
        id INTEGER NOT NULL,
        name VARCHAR(255),
        PRIMARY KEY (id)
);
CREATE TABLE sources (
        id INTEGER NOT NULL,
        phone_num VARCHAR(255),
        operation_system VARCHAR(255),
        group_id INTEGER,
        PRIMARY KEY (id),
        FOREIGN KEY(group_id) REFERENCES groups (id)
);
CREATE TABLE messages (
        id INTEGER NOT NULL,
        content VARCHAR(255) NOT NULL,
        receive_time DATETIME,
        data_type VARCHAR(255),
        source_id INTEGER,
        msg_metadata VARCHAR(255),
        PRIMARY KEY (id),
        FOREIGN KEY(source_id) REFERENCES sources (id)
);

-- Adding Data --
INSERT INTO groups (id, name)
VALUES
    (1, 'Monkey-Group'),
    (2, 'Submarine-Group');

INSERT INTO sources (id, phone_num, operation_system, group_id)
VALUES
    (1, '0509988999', 'Android', 1),
    (2, '0509223329', 'Iphone', 2),
    (3, '0526699881', 'Android', 1),
    (4, '0536449881', 'Iphone', 2);

INSERT INTO messages (id, content, receive_time, data_type, source_id, msg_metadata)
VALUES
    (1, 'I am coding', '2020-05-13 23:01:01', 'message', 2, ''),
    (2, 'Pokemon is fun', '2020-05-13 23:01:02', 'message', 2, ''),
    (3, 'Whats up?', '2020-05-13 23:01:04', 'message', 1, ''),
    (4, 'Fine...', '2020-05-13 23:01:05', 'message', 2, '');

